const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/home", (req, res) => {
	res.send(`Welcome to the home page`);
});

let users = [{
"username": "johndoe",
"password": "john1234",
}
]

app.get("/users", (req, res) => {
	res.send(users)
});

app.delete("/delete-user", (req, res) => {
	let message;
	for(let i=0; i<users.length; i++){
		if (req.body.username == users[i].username) {
			users.splice(i, 1);
			message = `User ${req.body.username} has been deleted.`;
			res.send(message)
			break;
		} 
	}
	if(!message){
		message = "User does not exist";
	} else {
		message = "No users found";
	}
	res.send(message)
});





if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;