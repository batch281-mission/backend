// CRUD Operations

// Create
// Inserting one collection
db.users.insertOne({
	"firstName": "Jane",
	"lastName" : "Doe",
	"age": 21,
	"contact": {
		"phone": "85479123",
		"email": "janedoe@mail.com"
	},
	"course":["CSS", "JavaScript", "Phyton"],
	"department": "none"
});

// Insert many collections
db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName" : "Hawking",
		"age": 76,
		"contact": {
			"phone": "85479123",
			"email": "stephenh@mail.com"
		},
		"course":["React", "PHP", "Phyton"],
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName" : "Armstrong",
		"age": 82,
		"contact": {
			"phone": "85479123",
			"email": "neilarms@mail.com"
		},
		"course":["React", "Laravel", "Sass"],
		"department": "none"
	}
]);

// Finding documents (Read)
// Find

// Retrieving all documents
db.users.find();

// Retrieving single documents
db.users.find({ "firstName": "Stephen"});

// Retrieving documents with multiple parameters
db.users.find({ "lastName": "Armstrong", "age": 82});

// Update a single document to update

// Creating a document to update
db.users.insertOne({
	"firstName": "Test",
	"lastName" : "Test",
	"age": 0,
	"contact": {
		"phone": "00000000",
		"email": "test@mail.com"
	},
	"course":[],
	"department": "none"
});

db.users.updateOne(
	{ "firstName": "Test"},
	{
		$set : {
			"firstName": "Bill",
			"lastName" : "Gates",
			"age": 65,
			"contact": {
				"phone": "87654321",
				"email": "bill@mail.com"
			},
			"course":["PHP", "Laravel", "HTML"],
			"department": "Operations",
			"status": "active"
		}
	}
);

// Updating multiple documents
db.users.updateMany(
	{"department": "none"},
	{
		$set : {"department": "HR"}
	}
);

// Replace one
db.users.replaceOne(
	{"firstName": "Bill"},
	{
		"firstName": "Bill",
		"lastName" : "Gates",
		"age": 65,
		"contact": {
			"phone": "87654321",
			"email": "bill@mail.com"
		},
		"course":["PHP", "Laravel", "HTML"],
		"department": "Operations"
	}
)

// Deleting documents (Delete)

// Creating a document to delete
db.users.insert({
	"firstName": "test"	
});

// Deleting a single document
db.users.deleteOne({
	"firstName": "test"
});

// Query and embedded document
db.users.find({
	"contact": {
		"phone": "85479123"
	}
})
