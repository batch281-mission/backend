const Product = require("../models/Product");

// Creating a product (Admin only)

module.exports.createProduct = (data) => {
	let newProduct = new Product({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price,
		image: data.product.image,
		category: data.product.category
	});

	return newProduct.save().then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};


// Retrieving all product

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Retrieving all ACTIVE product

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Retrieving a SINGLE product

module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Updating product information (Admin Only)

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		image: reqBody.image,
		category: reqBody.category,
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			console.log(product);
			return true;
		}
	})
};


// Archiving product (Admin Only)

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if(error){
			return false;
		} else {
			console.log(product);
			return true;
		}
	})
};


// Activating product (Admin Only)

module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if(error){
			return false;
		} else {
			console.log(product);
			return true;
		}
	})
};