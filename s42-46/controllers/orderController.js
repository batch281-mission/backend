const Product = require("../models/Product");
const Order = require("../models/Order");


// Creating Order (Non-admin checkout)

module.exports.createOrder = async (data) => {

	let product = await Product.findById(data.productId);

	if (product == null){
		return "Product not existing";
	}

	if (product.isActive == false){
		return "Product not active";
	}

	let newOrder = new Order({
		userId: data.userId,
		products: [{
			name: data.name,
			productId: data.productId,
			quantity: data.quantity
		}],
		totalAmount: data.quantity * product.price,
		purchasedOn: new Date()
	});

	return newOrder.save().then((order,error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
};


// Retrieving Authenticated User Order Details

module.exports.retrieveOrderDetails = (userId) => {
	return Order.find({userId}).then(result => {
		return result;
	})
};


// Retrieving All Order Details (Admin Only)

module.exports.retrieveAllOrders = () => {
	return Order.find({}).then(result => {
		return result;
	});
};


// Retrieving Single User Order Details (Admin Only)

module.exports.retrieveUserOrders = (userId) => {
  return Order.find({ userId: userId })
    .then(result => {
      return result;
    });
};