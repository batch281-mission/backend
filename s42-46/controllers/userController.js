const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration

module.exports.registerUser = (reqBody) => {
	// Finding if a match is found
	return User.find({email: reqBody.email}).then((users) => {
	// If email already exist (match is found)
		if (users.length > 0){
			return "Email already exists."

		// If the email is not existing yet
		} else {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				mobileNo: reqBody.mobileNo,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
			})

			return newUser.save().then((user, error) => {
				// User registration failed
				if(error){
					return false;

				// User registration successful
				} else {
					return true;
				}
			});
		}
	});
};


// User Authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// If the user does not exist (No match found)
		if(result == null){
			return "User does not exist";

		// If the user exist (Match found)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the password is correct/matches
			if (isPasswordCorrect){
				return { access: auth.createAccessToken(result)};
			} else {
				return false;
			}
		}
	});
};



// Retrieving User Details 

module.exports.retrieveUserDetails = (data) => {
	return User.findById(data.userId).then(result => {
		return result
	} )
};


// Setting User as An Admin (Admin Only)

module.exports.setAsAdmin = (reqParams) => {

	let updateAdminField = {
		isAdmin: true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((product, error) => {
		if (error){
			return false;
		} else {
			return true;
		}
	})
};

// Check if email exists

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true
		// No duplicate emails found
		// The user is not registerd in the database
		} else {
			return false
		};
	});
};

// Retrieve all users

module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};


// Retrieve user details for admin

module.exports.getSingleUser = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return result;
	});
};