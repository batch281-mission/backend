const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();


// Connecting to MongoDB
mongoose.connect("mongodb+srv://nuevemission:Hr87gofDXL7v73pO@wdc028-course-booking.a8kubnp.mongodb.net/Capstone2?retryWrites=true&w=majority", 
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
	);

let db = mongoose.connection;

db.on('error', console.error.bind(console,"MongoDB connection Error."));
db.once('open', () => console.log('Now connected to MongoDB Atlas!'));


// Allows all resource to access our backend application
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);




// Listening to PORT
app.listen(process.env.PORT || 4000, () =>{

	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})
