const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product
router.post("/createProduct", auth.verify, (req , res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	// To check if user is Admin
	// If user is admin
	if (data.isAdmin == true){
		productController.createProduct(data).then(resultFromController => res.send(resultFromController));
	// If user is not admin
	} else {
		res.send(false);
	}
});


// Route for retrieving all products
router.get("/all", (req, res) => {
	productController.getAllProduct().then(resultFromController => { console.log("Products retrieved from the backend:", resultFromController); res.send(resultFromController)});
});



// Route for retrieving all active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
})


// Route for updating product information (Admin only)
router.put("/update/:productId", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
});


// Route for archiving product (Admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
})

module.exports = router;


// Route for activating product (Admin only)
router.put("/:productId/activate", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true){
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
})

module.exports = router;