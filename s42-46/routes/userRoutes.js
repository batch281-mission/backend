const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req , res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// Route for retrieving user details
router.get("/details", auth.verify, (req , res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.retrieveUserDetails({userId : userData.id}).then(resultFromController => res.send(resultFromController))
})


// Route for setting user as an Admin (Admin Only)
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true){
		userController.setAsAdmin(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
})


// Route for checking if email already exists

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all users
router.get("/all", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true){
		userController.getAllUsers().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});


// Route for retrieving single user
router.get("/:userId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true){
		console.log(req.params.userId);
		userController.getSingleUser(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
})

module.exports = router;


