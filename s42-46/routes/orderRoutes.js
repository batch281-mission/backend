const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");


// Route for creating order

router.post("/checkout", auth.verify, async (req, res) => {
	let data = {
		name: req.body.name,
		productId: req.body.productId,
		quantity: req.body.quantity,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == false){
		orderController.createOrder(data).then(resultFromController => res.send(resultFromController))
		.catch(error => res.status(400).send(error));
	} else {
		res.send("You are an admin")
	}
});


// Route for retrieving authenticated user order details

router.get("/orderDetails", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	orderController.retrieveOrderDetails(userId).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all order details (Admin Only)

router.get("/allOrders", auth.verify, (req, res) => {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin == true){
		orderController.retrieveAllOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
});


// Route for retrieving single order details (Admin only)

router.get("/user/:userId", auth.verify, (req, res) => {
  console.log("Received request to retrieve single user orders.");
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  console.log("isAdmin:", data.isAdmin);
  console.log("userId:", req.params.userId);

  if (data.isAdmin === true) {
    orderController.retrieveUserOrders(req.params.userId)
      .then(resultFromController => res.send(resultFromController))
  } else {
    res.send(false);
  }
});


module.exports = router;