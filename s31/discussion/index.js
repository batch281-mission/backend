// Using require directive to load http module
let http = require("http");

// Create a server
http.createServer(function (request, response) {
	// Returning what type of response being thrown to the client
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Send the  response with the text content 'Hello, world!'
	response.end('Hello, again!')
}).listen(4000);

// When server is running, console will print the message:
console.log('Server is running at localhost:4000');