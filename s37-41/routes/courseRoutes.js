const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth")

// Route for creating a course
/*
router.post("/", (req, res) =>{
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})
*/

//Activity
// 1. Refactor the course route to implement user authentication for the admin when creating a course

/*router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    courseController
      .addCourse(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.status(401).send("You're not an admin to add course");
  }
});*/

// Allows us to export the "router" object that will be accessed in our index.js file

// Sir Tristan's Solution
// Route for creating a course
router.post("/", auth.verify, (req, res) => {
  
  const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin == true){
  courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }

});


// API DEVELOPMENT DAY 4

// Route for retrieving all the courses
router.get("/all", (req, res) => {
  courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the active courses
router.get("/", (req, res) => {
  courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})


// Router for retrieving a specific course
// Creating a course using "/:parametername" creates a dynamic route, meaning the url changes depending on the information provided
router.get("/:courseId", (req, res) => {
  console.log(req.params.courseId);

  // Since the course ID will be sent via the URL, we cannot retrieve it from the request body
  // We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
  courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
  courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});



// Activity s40

//My solution

/*router.patch("/:courseId", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin == true){
   courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
 } else {
  res.send(false);
 }
});
*/


// Sir Tristan's solution

// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {

  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin == true){
    courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }
  
});


module.exports = router;


