const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes")

const app = express();

// Connect to our MongoDB
mongoose.connect("mongodb+srv://nuevemission:Hr87gofDXL7v73pO@wdc028-course-booking.a8kubnp.mongodb.net/s37-41API?retryWrites=true&w=majority", 
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
	);


let db = mongoose.connection;

db.on('error', console.error.bind(console,"MongoDB connection Error."));
db.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// Allows all resource to access our backend application
app.use(cors());

app.use(express.json()); // adds express json middleware to our app, allows us to parse incoming request
app.use(express.urlencoded({extended:true})) // enables us to parse nested object and forms (it handles data that will be coming from our client)

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000, () =>{

	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})